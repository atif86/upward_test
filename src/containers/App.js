import React, { Component } from 'react';
import LoginForm from '../components/LoginForm';
import UserProfile from '../components/UserProfile';
import './App.css';

class App extends Component {

  constructor() {
    super();
    this.state = {
      userData: null,
      loading: false
    }
  }

  componentWillMount() {
    const userData = sessionStorage.getItem('userData');
    const parsedData = JSON.parse(userData);
    if(parsedData) {
      this.setState({
        userData: parsedData
      })
    }
  }

  login() {
    this.setState({
      loading: true
    })
    const url = 'http://ec2-54-69-219-242.us-west-2.compute.amazonaws.com:8000/v1/customer/login/';
    const data = {
      email: document.getElementById('email').value,
      password: document.getElementById('password').value
    }
    let fetchData = {
      method: 'POST',
      body: data,
      headers: new Headers()
    }
    fetch(url, fetchData)
    .then((res) => res.json())
    .then((result) => {
      sessionStorage.setItem('userData', JSON.stringify(result.data));
      this.setState({
        loading: false,
        userData: result.data
      })
    })
  }

  clearData() {
    sessionStorage.removeItem('userData');
    this.setState({
      userData: null
    })
  }

  render() {
    return (
      <div style={{ backgroundColor: '#4bd4dd', backgroundImage: '-webkit-linear-gradient(-20deg, #1c4dd0 50%, #4bd4dd 50%)', height: '100vh'}}>
        <div className="container" style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '100vh'}}>
          <div className="page">
            {!this.state.userData &&
              <LoginForm
                loading={this.state.loading}
                login={() => this.login()}
              />
            }
            {this.state.userData &&
              <UserProfile
                userData={this.state.userData}
                clearData={() => this.clearData()}
              />
            }
          </div>
        </div>
      </div>
    );
  }
}

export default App;
