import React from 'react';
import { Button, Input } from 'semantic-ui-react';

const LoginForm = (props) => (
  <div style={{ display: 'flex', flexDirection: 'column' }}>
    <div className="image">
      <img src="/assets/calendar.png" alt="login_image" />
    </div>
    <p className="login-text">LOGIN</p>
    <Input id="email" placeholder='Email' style={{ margin: 10 }} />
    <Input id="password" type="password" placeholder='Password' style={{ margin: 10 }} />
    <Button loading={props.loading} onClick={() => props.login()} style={{ margin: 10, backgroundImage: '-webkit-linear-gradient(-20deg, #1c4dd0 50%, #4bd4dd 50%)', color: '#fff' }}>
      Login To Profile
    </Button>
  </div>
)

export default LoginForm;
