import React from 'react';
import { Icon, Button } from 'semantic-ui-react'

const UserProfile = (props) => (
  <div style={{ display: 'flex', flexDirection: 'column', position: 'relative', height: '100%' }}>
    <div className="user-image">
      <img src={props.userData.pic} alt="user_pic" style={{ width: 100, height: 100, borderRadius: 50 }}/>
    </div>
    <div className="content">
      <Icon name='user' className="icon" />
      <p style={{ width: '55%' }}>{props.userData.username}</p>
    </div>
    <div className="content">
      <Icon name='file text' className="icon" />
      <p style={{ width: '55%' }}>{props.userData.bio}</p>
    </div>
    <div className="content">
      <Icon name='mail' className="icon" />
      <p style={{ width: '55%' }}>{props.userData.email}</p>
    </div>
    <div style={{ position: 'absolute', bottom: 10, right: 10 }}>
      <Button
        icon='remove'
        content="Clear Data"
        labelPosition='left'
        style={{ backgroundColor: '#e74c3c', color: '#fff' }}
        onClick={() => props.clearData()}
      />
    </div>
  </div>
)

export default UserProfile;
